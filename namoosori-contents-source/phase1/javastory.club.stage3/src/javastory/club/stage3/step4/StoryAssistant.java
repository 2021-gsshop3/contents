package javastory.club.stage3.step4;

import javastory.club.stage3.step1.entity.club.CommunityMember;
import javastory.club.stage3.step1.entity.club.TravelClub;
import javastory.club.stage3.step4.da.map.ClubStoreMapLycler;
import javastory.club.stage3.step4.da.map.io.MemoryMap;
import javastory.club.stage3.step4.logic.ServiceLogicLycler;
import javastory.club.stage3.step4.service.ClubService;
import javastory.club.stage3.step4.service.MemberService;
import javastory.club.stage3.step4.service.ServiceLycler;
import javastory.club.stage3.step4.service.dto.ClubMembershipDto;
import javastory.club.stage3.step4.service.dto.MemberDto;
import javastory.club.stage3.step4.service.dto.TravelClubDto;
import javastory.club.stage3.step4.ui.menu.MainMenu;

public class StoryAssistant {
	//
	private void startStory() {
		//
		MainMenu mainMenu = new MainMenu();
		mainMenu.show();
	}

	public static void main(String[] args) {
		//
		TravelClubDto sampleClubDto = new TravelClubDto("JavaTravelClub", "Travel club to the Java island.");
		MemberDto sampleMemberDto = new MemberDto("mymy@nextree.co.kr", "Minsoo Lee", "010-3321-1001");

		ServiceLycler lycler = ServiceLogicLycler.shareInstance();
		ClubService clubService = lycler.createClubService();
		MemberService memberService = lycler.createMemberService();

		clubService.register(sampleClubDto);
		memberService.register(sampleMemberDto);

		clubService.addMembership(new ClubMembershipDto("00001", "mymy@nextree.co.kr"));

		StoryAssistant assistant = new StoryAssistant();
		assistant.startStory();

	}
}
